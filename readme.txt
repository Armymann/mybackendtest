Развернуть окружение:
docker-compose up -d --build

http://localhost:8000/admin/ - админка django
http://localhost:5555/ - страница flower для celery
http://localhost:8000/api/schema/swagger-ui/ - UI для тестирования API


Регистрация (с письмом активации на указанную почту):
    Request:
        URL: http://localhost:8000/auth/users/
        Method: POST
        Request body:
            {
              "first_name": "string",
              "last_name": "string",
              "middle_name": "string",
              "email": "user@example.com",
              "username": "string",
              "password": "string"
            }
    Server response:
        Code 201
        Response body:
            {
              "first_name": "string",
              "last_name": "string",
              "middle_name": "string",
              "username": "string",
              "email": "user@example.com"
            }

Активация аккаунта:
    1 Вариант
        Request:
            URL: http://localhost:8000/auth/users/activation/
            Method: POST
            Request body:
                {
                  "uid": "string",
                  "token": "string"
                }
        Server response:
            Code 204

    2 Вариант
        Request:
            URL: http://localhost:8000/auth/users/activation/{uid}/{token}/
            Method: GET
        Server response:
            Code 200

Сброс пароля (1 шаг - запрос на сброс):
    Request:
        URL: http://localhost:8000/auth/users/reset_password/
        Method: POST
        Request body:
            {
              "email": "user@example.com"
            }
    Server response:
        Code 204

Сброс пароля (2 шаг - подтверждение и ввод нового пароля):
    Request:
        URL: http://localhost:8000/auth/users/reset_password_confirm/
        Method: POST
        Request body:
            {
              "uid": "string",
              "token": "string",
              "new_password": "string"
            }
    Server response:
        Code 204

Авторизация по JWT:
    Request:
        URL: http://localhost:8000/auth/token/
        Метод: POST
        Request body:
            {
              "username": "string",
              "password": "string"
            }
    Server response:
        Code 200
        Response body:
            {
              "access": "string",
              "refresh": "string"
            }
