from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import SpectacularSwaggerView, SpectacularAPIView
from rest_framework_simplejwt.views import TokenObtainPairView

from mainapp.views import ActivateUser

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include('mainapp.urls')),
    path('auth/', include('djoser.urls')),
    path('auth/users/activation/<uid>/<token>/', ActivateUser.as_view()),
    path('auth/token/', TokenObtainPairView.as_view()),
    path('api/schema/', SpectacularAPIView.as_view(), name='schema'),
    path('api/schema/swagger-ui/', SpectacularSwaggerView.as_view(url_name='schema'), name='swagger-ui'),

]
